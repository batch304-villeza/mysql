-- [SECTION I] Insert
-- Add New Records
-- Add 5 Artists, 2 albums each, 2 song per album

-- 1. artists table
INSERT INTO artists(name) VALUES ("Talor Swift");
INSERT INTO artists(name) VALUES ("Lady Gaga");
INSERT INTO artists(name) VALUES ("Justin Bieber");
INSERT INTO artists(name) VALUES ("Ariana Grande");
INSERT INTO artists(name) VALUES ("Bruno Mars");


-- 2. albums
INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Fearless", "2008-01-01", 6);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Red", "2012-01-01", 6);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("A Star is Born", "2018-01-01", 7);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Born This Way", "2011-01-01", 7);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Purpose", "2012-01-01", 8);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Believe", "2012-01-01", 8);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Dangerous Woman", "2016-01-01", 9);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Thank U, Next", "2019-01-01", 9);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("24K Magic", "2016-01-01", 10);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Earth to Mars", "2012-01-01", 10);


-- 3. songs
-- Taylor
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Fearless", 246, "Pop Rock", 6);
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Love Story", 213, "Country Pop", 6);

INSERT INTO songs(song_name, length, genre, album_id)
VALUES("State of Grace", 243, "Rock, Alternative Rock", 7);
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Red", 204, "Country", 7);

-- Lady Gaga
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Black Eyes", 141, "Rock And Roll", 8);
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Shallow", 201, "Country, Rock, Folk Rock", 8);

INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Born This Way", 252, "Electropop", 9);
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Marry The Night", 201, "Electropop", 9);

-- Justin
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Sorry", 152, "Dancehall-poptropical house moombahton", 10);

INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Boyfriend", 251, "Pop", 11);

-- Ariana
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Into You", 242, "EDM House", 12);

INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Thank U, Next", 146, "Pop, R&B", 13);

-- Bruno Mars
INSERT INTO songs(song_name, length, genre, album_id)
VALUES("24K Magic", 204, "Funk, Disco, R&B", 14);

INSERT INTO songs(song_name, length, genre, album_id)
VALUES("Lost", 132, "Pop", 15);




-- [Section II] Advance Selects

-- 1. comparison operator
-- a. Greater Than
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE length > 250;

-- b. Less Than
SELECT * FROM songs WHERE id < 11;

-- c. Equal

-- d. Not Equal / Exclude
SELECT * FROM songs WHERE id != 11;
SELECT * FROM songs WHERE genre != "OPM";


-- 2. Logical Operator
-- OR Operator - Get Specific songs through ID (OR)
SELECT * FROM songs WHERE id = 1 OR  id = 3 OR  id = 5;

-- IN Operator
SELECT * FROM songs WHERE id IN (6, 5, 9);
SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");
SELECT * FROM songs WHERE genre IN ("Pop", "R&B");


-- 3. And Operator - Combine Conditions
SELECT * FROM songs WHERE aalbum_id = 4 AND id < 8;

-- 4. Like Operator - Find Partial Matches
-- start
SELECT * FROM songs WHERE song_name LIKE "a%";
-- end
SELECT * FROM songs WHERE song_name LIKE "%a";
-- in between
SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM albums WHERE date_released LIKE "201_%";
SELECT * FROM albums WHERE album_title LIKE "Pur_ose";
SELECT * FROM albums WHERE album_title LIKE "Pur_o_e";

-- 5. Sorting Records
SELECT * FROM songs ORDER BY song_name DESC;
SELECT * FROM songs ORDER BY song_name ASC;

-- 6. Distinct Records
SELECT DISTINCT genre FROM songs;

-- 7. BINARY - Case Sensitive Condition
SELECT * FROM songs WHERE song_name LIKE BINARY "b%";


-- [SECTION] Joining Tables
-- Cobine artists and albums table
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artists_id

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artists_id
	JOIN songs ON albums.id = songs.album_id

-- select columns to be included per table
SELECT artists.name, albums.album_title FROM artists
	JOIN albums ON artists.id = albums.artists_id