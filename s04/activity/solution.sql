-- S04 Activity:
	-- 1. Create an solution.sql file inside s04/activity project and do the following using the music_db database:
	
	-- a. Find all artists that has letter d in its name.
		SELECT id, name FROM music_db.artists WHERE name LIKE "%d%";

	-- b. Find all songs that has a length of less than 3:50.
		SELECT id, song_name, length, genre, album_id
			FROM music_db.songs WHERE length < 350;

	-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
		SELECT a.album_title, s.song_name, s.length
			FROM music_db.albums a
			JOIN music_db.songs s ON a.id = s.album_id;
	
	-- d. Join the 'artists' and 'albums' tables. (Find all artists that has letter a in its name.)
		SELECT ar.id, ar.name, al.id, al.album_title, al.date_released, al.artists_id
			FROM music_db.artists ar
			JOIN music_db.albums al ON ar.id = al.artists_id
			WHERE ar.name LIKE "%a%";
	
	-- e. Sort the albums in Z-A order. (Show only the first 4 records.)
		SELECT id, album_title, date_released, artists_id FROM music_db.albums ORDER BY album_title DESC LIMIT 4;

	-- f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
		SELECT a.id, a.album_title, a.date_released, a.artists_id, s.id, s.song_name, s.length, s.genre, s.album_id
			FROM music_db.albums a
			JOIN music_db.songs s ON a.id = s.album_id
			ORDER BY a.album_title DESC;

