-- 1.
	SELECT customerName
	FROM classic_models.customers
	WHERE country = "Philippines";

-- 2.
	SELECT contactLastName, contactFirstName
	FROM classic_models.customers
	WHERE customerName = "La Rochelle Gifts";

-- 3.
	SELECT productName, MSRP
	FROM classic_models.products
	WHERE productName = "The Titanic";

-- 4.
	SELECT firstName, lastName
	FROM classic_models.employees
	WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
	SELECT customerName
	FROM classic_models.customers
	WHERE state IS NULL;

-- 6.
	SELECT firstName, lastName, email
	FROM classic_models.employees
	WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
	SELECT customerName, country, creditLimit
	FROM classic_models.customers
	WHERE country != "USA" AND creditLimit > 3000;

-- 8.
	SELECT customerNumber
	FROM classic_models.orders
	WHERE comments LIKE "%DHL%";

-- 9.
	SELECT productLine
	FROM classic_models.productlines
	WHERE textDescription LIKE "%state of th art%";

-- 10.
	SELECT DISTINCT country
	FROM classic_models.customers;

-- 11.
	SELECT DISTINCT status
	FROM classic_models.orders;

-- 12.
	SELECT customerName, country
	FROM classic_models.customers
	WHERE country IN ("USA", "France", "Canada");

-- 13.
    SELECT e.firstName, e.lastName, o.city AS officeCity
	FROM classic_models.employees e
	JOIN classic_models.offices o
		ON e.officeCode = o.officeCode
	WHERE o.city = "Tokyo";
	
-- 14.
	-- Using Join Statement
	SELECT c.customerName
	FROM classic_models.customers c
	JOIN classic_models.employees e ON c.salesRepEmployeeNumber = e.employeeNumber
	WHERE CONCAT_WS(" ",e.firstName, e.lastName) = "Leslie Thompson";
    
    -- Using Subquery
    SELECT customerName
    FROM classic_models.customers
    WHERE salesRepEmployeeNumber = (SELECT employeeNumber FROM classic_models.employees WHERE CONCAT_WS(" ",firstName, lastName) = "Leslie Thompson");

-- 15.
	SELECT p.productName, c.customerName
	FROM classic_models.orders o
	JOIN classic_models.customers c
		ON o.customerNumber = c.customerNumber
	JOIN classic_models.orderdetails od
		ON o.orderNumber = od.orderNumber
	JOIN classic_models.products p
		ON od.productCode = p.productCode
	WHERE c.customerName = "Baane Mini Imports";
    
-- 16.
	SELECT	DISTINCT e.firstName, e.lastName, c.customerName, eo.country
    FROM classic_models.customers c
	JOIN classic_models.employees e
		ON c.salesRepEmployeeNumber = e.employeeNumber
	JOIN classic_models.offices eo
		ON e.officeCode = eo.officeCode
	JOIN classic_models.orders o
		ON o.customerNumber = c.customerNumber
	WHERE c.country = eo.country;

-- 17.
	SELECT productName, quantityInStock
	FROM classic_models.products
	WHERE productLine = "planes" AND quantityInStock < 1000;

-- 18.
	SELECT customerName
	FROM customers
	WHERE phone LIKE "%+81%";
