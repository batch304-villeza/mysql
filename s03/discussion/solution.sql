-- CRUD Operation
USE music_db;


-- [SECTION I] Insert Statement (Create)

-- Insert to artists table
INSERT INTO artists (name) VALUES("Rivermaya");
INSERT INTO artists (name) VALUES("Psy");
INSERT INTO artists (name) VALUES("Boys Like Girls");
INSERT INTO artists (name) VALUES("FM Static");
INSERT INTO artists (name) VALUES("Seconhand Serenade");

SELECT id, name FROM artists;

-- Insert to albums table
INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Psy 6","2012-01-01",2);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Trip","1996-01-01",1);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("BLG","2007-01-01",3);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("EMO","2008-01-01",4);

INSERT INTO albums (album_title, date_released, artists_id)
VALUES ("Serenade","2010-01-01",5);

SELECT * FROM albums;


-- Insert to songs table
INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Gangnam Style", 253, "K-Pop", 2);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Kisapmata", 279, "OPM", 1);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Thunder", 279, "Rock", 3);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Moment of Truth", 279, "Rock", 4);

INSERT INTO songs (song_name, length, genre, album_id)
VALUES ("Fall For You", 279, "Rock", 5);

SELECT * FROM songs;



-- [SECTION II] Select Statement - (Read)

-- 1. Select specific column from the table
SELECT song_name, genre FROM songs;

-- 2. Select the entire columns from the tables
SELECT * FROM songs;

-- 3. Select with WHERE Clause
SELECT song_name from songs WHERE genre = "OPM";

-- 4. 
SELECT song_name, length FROM Songs WHERE length > 240 AND genre = "K-Pop";


-- 5. We can use AND and OR for multiple expressions in the WHERE clause
SELECT song_name, length FROM songs WHERE length > 240 AND genre = "K-Pop";
SELECT song_name, length FROM songs WHERE length > 240 OR genre = "OPM";



-- [SECTION III] Update Statement - (UPDATE)
-- 1. Update the length of Gangnam Style to 240
UPDATE songs SET length = 240 WHERE song_name = "Gangnam Style";
-- Note: Removing the WHERE clause will update all rows



-- [SECTION IV] Delete Statement - (Delete)
-- 2. 
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
-- Note: Removing the WHERE clause will delete all rows